from BigDFT.RemoteRunnerUtils import CallableAttrDict
from BigDFT.URL import URL

class SpringScript(CallableAttrDict):
    def __str__(self):        
        top = "#!/bin/sh\n"
        top += "#PBS -S /bin/bash\n"
        top += "#PBS -q " + self.queue + "\n"
        
        if self.queue in ["winter1", "winter2"]:
            top += "#PBS -l nodes=1:ncpus=36\n"
        elif self.queue in ["winter3"]:
            top += "#PBS -l nodes=1:ncpus=44\n"
        
        middle = "cd $PBS_O_WORKDIR\n"
        middle += "export OMP_NUM_THREADS=" + str(self.omp) + "\n"
        middle += "eval \"$(conda shell.bash hook)\"\n"
        middle += "conda activate remote_bigdft\n"
        middle += "source " + self.prefix + "/bin/bigdftvars.sh\n"
        
        bottom = "export BIGDFT_MPIRUN=\"mpiexec.hydra -machinefile $PBS_NODEFILE -n " + str(self.mpi) + \
                 " -perhost " + str(self.mpi) + "\""
        
        return top + middle + bottom
    

submission_script = SpringScript()
submission_script.prefix = "/home/dawson/binaries/bdft/install"
submission_script.submitter = "qsub"

url = URL(host="spring.r-ccs27.riken.jp", user="dawson")