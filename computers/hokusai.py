from BigDFT.RemoteRunnerUtils import CallableAttrDict
from BigDFT.URL import URL

class HokusaiScript(CallableAttrDict):
    def __str__(self):
        ppn = int(self.mpi/self.number_of_nodes)
        top = "#!/bin/sh\n"
        top += "#PJM -L rscunit=bwmpc\n"
        top += "#PJM -L rscgrp=batch\n"
        top += "#PJM -L vnode=" + str(self.number_of_nodes) + "\n"
        top += "#PJM -L vnode-core=40\n"
        top += "#PJM -L elapse=" + self.time + "\n"
        top += "#PJM -g " + self.project + "\n"
        top += "#PJM -j\n"
        
        middle = "source ~/.bashrc\n"
        middle += 'export BIGDFT_MPIRUN="mpirun -np ' + \
                  str(self.number_of_nodes*ppn) + \
                  ' -ppn ' + str(ppn) + '"\n'
        middle += "source " + self.prefix + "/bin/bigdftvars.sh\n"
        middle += "export OMP_NUM_THREADS=" + str(self.omp) + "\n"    
        middle += "conda activate remote_bigdft\n"
        
        return top + middle
    

submission_script = HokusaiScript()
submission_script.prefix = "/home/wddawson/binaries/bdft/install"
submission_script.submitter = "pjsub"
submission_script.time = "1:00:00"

url = URL(host="hokusai.riken.jp", user="wddawson", verbose=True)
remote_dir = "/home/wddawson/runs/2022/remote_pictures"