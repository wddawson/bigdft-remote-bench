def run_bigdft(name, posinp, **kwargs):
    """The basic function to run a calculation.

    This function executes the SystemCalculator class with a provided keyword arguments.
    It creates a tarfile which includes the logfile of the run as well as
    the time.yaml performance file associated. The name of the tarfile is
    `<name>.tar.gz`.

    Args:
        posinp (path): the posinp file
        name (str): the name of the run
        **kwargs: a dictionary following the same API of SystemCalculator class

    Returns:
        The total estimated memory of the calculation in the case of a `dry_run`,
        otherwise the total energy.
    """
    from BigDFT.Calculators import SystemCalculator
    from os.path import join, isfile
    from futile.Utils import create_tarball
    code = SystemCalculator()
    log = code.run(name=name, posinp=posinp, **kwargs)
    jobname = name
    target = jobname+'.tar.gz'
    dry_run = kwargs.get('dry_run', False)
    files = ['log-' + jobname + '.yaml']
    if dry_run:
        result = log.log['Estimated Memory Peak (MB)']
        print(log.log['Total Number of Orbitals'], result)
    else:     
        result = log.energy
        print(result)
        if log.timefile is not None:
            files += [log.timefile]
    create_tarball(target, files=files)
    return result


def input_template(linear=False):
    import BigDFT.Inputfiles as I
    import yaml
    new = """
import: linear
perf:
  check_sumrho: 0
  check_overlap: 0
lin_general:
  charge_multipoles: 0
  output_mat: 0
"""
    lin_dict = yaml.safe_load(new)
    # Basic Input Parameters 
    inp = I.Inputfile()
    inp.set_xc("PBE")
    inp.set_hgrid(0.5)
    inp.set_rmult(coarse=5.0, fine=7.0) 
    # Get the PSP directory
    inp.set_psp_nlcc()   
    if linear:
        inp.update(lin_dict)
    return inp

    
def campaign(run_spec, **kwargs):
    """
    Create a list of dictionary of runs to be passed to a Remotemanager Dataset
    for a set of benchmark specifications.
    
    Args:
        run_spec (dict): dictionary of the runs organized as {filepath: spec}
           where **spec is the set of keyword arguments which have to be passed
           to the `append_run` method of the dataset.
           `filepath` should be the relative path of the input file to be executed.
           The filepath is taken as posinp value unless ``posinp`` is specified
           in the **kwargs.
           Alternatively, if the argument 'name' is not specified in the **kwargs,
           the dictionary is assumed to take the form {name: spec}. Clearly in this case
           the value 'posinp' should be in the spec dictionary.
           If both 'name' and 'posinp' are in the spec dictionary, the key of the
           dictionary is not employed.
        **kwargs: extra arguments to be given to the `run_bigdft` function.

    Returns:
        list: the list of runner arguments to be given to the creation of the RemoteManager dataset.

    """
    from os.path import basename, dirname, join, isfile
    runner_args = []
    for file, runs in run_spec.items():
        filename = basename(file)
        extra_file = [file]
        for run in runs:
            if 'posinp' in run:
                posinp = run.pop('posinp')
                if isinstance(posinp, str) and isfile(posinp):
                    filename = basename(posinp)
                    extra_file = [posinp]
                else:
                    filename = posinp
                    extra_file = []
            func_args = {'posinp': filename}
            name = run['name'] if 'name' in run else file
            func_args['name'] = name
            func_args.update(**kwargs)
            #print(file, run)
            args = dict(extra_files_send=extra_file,
                        extra_files_recv=[name + '.tar.gz'],
                        jobname=name,
                        arguments=func_args, **run)
            runner_args.append(args)
    return runner_args


def load_campaign(campaign_spec):
    from remotemanager import Dataset
    from yaml import safe_load
    cp = safe_load(campaign_spec)
    campaign = create_campaign(cp.pop('campaign'))
    host = {k: cp.pop(k) for k in ['computer', 'platform', 'environment']}
    url = RM.get_computer(**host)
    ds = Dataset(url=url, **cp)
    ds.wipe_runs() # remove any accidentally present run
    for run in campaign:
        ds.append_run(lazy=True, **run)
    ds.finish_append()
    return ds


def create_campaign(cp):
    # custom template argument
    if 'input_template' in cp:
        template_kw = cp.pop('input_template')
        if template_kw is None:
            template_kw = {}
        cp.setdefault('input', {}).update(input_template(**template_kw))
    run_spec = cp.pop('run_spec')
    return campaign(run_spec,**cp)
    

# def log_performance_info(log):
#     from numpy import nan
#     ks = {'Hostname': 'Root process Hostname',
#           'Date': 'Timestamp of this run',
#           'MPI':'Number of MPI tasks',
#           'OMP': 'Maximal OpenMP threads per MPI task',
#           'Mem': 'Memory Consumption Report',
#           'Walltime': 'Walltime since initialization',
#           'SF': 'Total No. Support Functions',
#           'Orbitals': 'Total Number of Orbitals',
#           'Electrons': 'Total Number of Electrons'
#           }
#     df = {k: log.log.get(v,nan) for k,v in ks.items()}
#     if ks['Mem'] in log.log:
#         df['Mem'] = float(df['Mem']['Memory occupation']['Memory Peak of process'].rstrip('MB'))
#     df['Nat'] = len(log.astruct['positions'])
#     df['cores']=df['MPI']*df['OMP']
#     df['TotMem']=df['MPI']*df['Mem']
#     df['NodeMem']=df.get('MPI tasks of root process node', 1)*df['Mem']
#     df['CPUhours']=df['cores']*df['Walltime']/3600.0
#     df['CPUmin/at']=df['CPUhours']*60.0/df['Nat']
#     df['Memory/at']=df['TotMem']/df['Nat']
#     return df

def run_spec_from_log(log):
    from BigDFT.Inputfiles import Inputfile
    from os.path import isfile
    from BigDFT.Systems import system_from_log, system_from_dict_positions
    inp = Inputfile.from_log(log)
    posinp_log = system_from_log(log).get_posinp()
    if 'posinp' in inp:
        sys = system_from_dict_positions(**inp.pop('posinp'))
        posinp = sys.get_posinp()
        if isinstance(posinp, str) and not isfile(posinp):
            posinp = posinp_log
    else:
        posinp = posinp_log
    return inp, posinp


def campaign_spec_from_logs(lfs):
    """Identify the run_spec and input parameters from a set of logfile runs."""
    campaigns = {}
    names = {}
    for k, v in lfs.items():
        for l in v:
            inp, posinp = run_spec_from_log(l)
            found =False
            for k1, v in campaigns.items():
                found = inp == v['input']
                if found: 
                    break
            if not found:
                k1 = k
            perf = l.get_performance_info()
            name = l.log['radical']
            if name is None:
                name = ''
            mpi = perf['MPI']
            omp = perf['OMP']
            if name in names.get(k1, set()):
                name = '-'.join(map(str,[name,omp,mpi]))
            if name in names.get(k1, set()):
                name += '-new'
            names.setdefault(k1, set()).add(name)
            campaigns.setdefault(k1,
                                 {'input':inp}).setdefault('run_spec',
                                                           {}).setdefault(name,[]).append(
                                                                          {'omp': omp,
                                                                           'mpi': mpi,
                                                                           'posinp':posinp})
    return campaigns

def log_spec(filename, timefile=None, log=None):
    from BigDFT.Logfiles import Logfile
    from futile import YamlIO
    if log is None:
        log = Logfile(filename)
    timefile = log.timefile
    wfn_opt = None
    if timefile is not None:
        dt = YamlIO.load(timefile)[-1]
        if dt is not None:
            wfn_opt = dt.get('SUMMARY',{}).get('WFN_OPT',[None, None])[1]
    data = {'name': log.log['radical'], 'timefile': timefile, 'WFN_OPT': wfn_opt}
    if hasattr(log, 'energy'):
        data['energy'] = log.energy
    # data.update(log_performance_info(log))
    data.update(log.get_performance_info())
    return data


def create_strong_scaling(functions,starting_from,ending_to,max_tasks=None):
    minnodes = int((functions - 1 ) / starting_from) + 1
    maxnodes = int((functions - 1) / ending_to) + 1
    if max_tasks is not None:
        maxnodes = min(maxnodes, max_tasks)
    runs = []
    nodes=minnodes
    # print(nodes,maxnodes,minnodes)
    while nodes <= maxnodes:
        # print(nodes,maxnodes)
        runs.append(nodes)
        nodes *= 2
    return runs


def create_bench_spec(dft,cores_per_node,omps,max_nodes,fragment_size,fill_node=True):
    from os.path import basename, splitext
    bench_spec = {}
    for file,val in zip(dft['name'],dft['Orbitals']):
        #print(file)
        runs=[]
        # at least one fragment
        minval = max(cores_per_node*4, fragment_size)
        for omp in omps:
            mpi_per_node_max=int((cores_per_node -1)/omp) +1
            mpi_per_node = mpi_per_node_max if fill_node else 1
            while mpi_per_node <= mpi_per_node_max:
                # print(mpi_per_node,omp, val, minval,max_nodes)
                for nodes in create_strong_scaling(val,minval,cores_per_node,max_tasks=max_nodes):
                    mpi= nodes * mpi_per_node
                    # print(omp,mpi,cores_per_node)
                    if ((omp*mpi)%cores_per_node ==0 and cores_per_node > 1) or omp*mpi <= cores_per_node*nodes:
                        size = str(mpi)+'-'+str(omp)
                        name = file + '-' + size
                        runs.append({'omp':omp, 'mpi':mpi, 'name': name})
                mpi_per_node *= 2
        bench_spec[file] = runs
    return bench_spec


def dataset_args(name, remote_basedir):
    from os.path import join
    return dict(name=name,local_dir=name,
                remote_dir=join(remote_basedir,name),
                dbfile=name+'.yaml')    


def df_select(df, select_dict):
    dft = df
    for column, pattern in select_dict.items():
        if isinstance(column, int):
            dft = dft.filter(like=pattern, axis=column)
            continue
        if isinstance(pattern, str):
            dft = dft[dft[column].str.contains(pattern)]
        else:
            dft = dft[dft[column] == pattern]
    return dft


def extract_results(directory):
    from futile.Utils import file_list
    import tarfile
    for archive in file_list(directory,suffix='.tar.gz',
                             exclude='files',  # old RemoteRunner
                             include_directory_path=True):
        arch = tarfile.open(archive)
        arch.extractall(path=directory)
        arch.close()
    return get_logfile_list(directory)


def get_extracted_files(directory):
    from futile.Utils import file_list
    from os.path import join
    import tarfile
    remove = []
    archives = file_list(directory,suffix='.tar.gz',
                         exclude='files',  # old RemoteRunner
                         include_directory_path=True)
    for archive in archives:
        arch = tarfile.open(archive)
        remove += [join(directory,name) for name in arch.getnames()]
        arch.close()      
    logfiles = get_logfile_list(directory)
    keep = archives + [log for log in logfiles if log not in remove]
    return keep, remove


def get_remotemanager_files(directory):
    from futile.Utils import file_list
    remove = []
    for suffix in ['run.py', 'error.out', 'jobscript.sh',
                   'master.sh', 'repo.py', 'result.json',
                   'repo.py','repo.sh',
                   'function-files.tar.gz','run.sh']:
        remove += file_list(directory,suffix=suffix,
                            include_directory_path=True)
    return remove
    

def get_logfile_list(directory):
    from futile.Utils import file_list
    # this should be modified to recursively check the log in the subdirectories
    return file_list(directory=directory,prefix='log',
                     exclude='logfiles', # avoid subdirectories
                     suffix='.yaml',include_directory_path=True)


def lfs_add_files(files):
    from os import system
    for f in files:
        system('git-lfs track '+f)
        system('git add '+f)
        

def dataframe(allogs):
    from pandas import DataFrame
    fulldata = {}
    for logfile in allogs:
        try:
            fulldata[logfile] = log_spec(logfile)
        except Exception as e:
            print(logfile, str(e))
    df = DataFrame(fulldata).T
    return df

def timefile_data(timefiles,mask=None,**kwargs):
    from futile.Time import TimeData
    from numpy import array
    tt=TimeData(*timefiles,widgets=False,**kwargs)
    if mask is not None:
        dt = [(k,v[mask]) for k, v in tt.actual_data[0]]
        cats = array(tt.actual_data[1])[mask]
    else:
        dt = [(k,v) for k, v in tt.actual_data[0]]
        cats = array(tt.actual_data[1])
    return (dt,cats)
    

def draw_barplot(ids,data,width=0.8,shift=0,x=None,ax=None,
                 labels=None, annotations=None, aggregate=None, colors=None):
    from matplotlib import pyplot as plt
    from futile.Time import aggregate_names
    prop_cycle = plt.rcParams['axes.prop_cycle']
    if colors is None:
        colors = prop_cycle.by_key()['color']
    if ax is None:
        fig, ax = plt.subplots()
    bottom=0
    n=0
    dt = data[0] if aggregate is None else aggregate_names(data[0],aggregate)
    for i, (cat, vals) in enumerate(dt):
        if x is None:
            n = max(n, len(vals))
            x = [t for t in range(n)]            
        xs = [t + shift for t in x]
        bar=ax.bar(xs,vals,label=cat,bottom=bottom,width=width,color=colors[i%len(colors)])
        bottom += vals
    ax.set_xticks(x,ids)
    if labels is not None:
        plt.bar_label(bar,labels=labels if labels is not True else None)
    if annotations is not None:
        for lb,xt,yt in zip(annotations,xs,data[1]):
            ax.annotate(lb,(xt,yt),ha='center',va='bottom')#,rotation='vertical')
    return ax


def add_label_band(ax, axis, end, start, label, *, spine_pos=-0.05, tip_pos=-0.02):
    """
    Helper function to add bracket around y or x  -tick labels.

    Adapted from ``https://stackoverflow.com/questions/67235301/vertical-grouping-of-labels-with-brackets-on-matplotlib``

    Parameters
    ----------
    ax : matplotlib.Axes
        The axes to add the bracket to

    axis : str
        The axis of the axes, x or y

    end, start : floats
        The positions in *data* space to bracket on the y-axis

    label : str
        The label to add to the bracket

    spine_pos, tip_pos : float, optional
        The position in *axes fraction* of the spine and tips of the bracket.
        These will typically be negative

    Returns
    -------
    bracket : matplotlib.patches.PathPatch
        The "bracket" Aritst.  Modify this Artist to change the color etc of
        the bracket from the defaults.

    txt : matplotlib.text.Text
        The label Artist.  Modify this to change the color etc of the label
        from the defaults.

    """
    import matplotlib.path as mpath
    import matplotlib.patches as mpatches

    mean = (end+start) / 2
    if axis == 'y':
        # grab the yaxis blended transform
        transform = ax.get_yaxis_transform()
        shape = [[tip_pos, end],
                [spine_pos, end],
                [spine_pos, start],
                [tip_pos, start],]
        rotation = 'vertical'
        xtext = spine_pos
        ytext = mean
        ha="right"
        va="center"        
    if axis == 'x':
        transform = ax.get_xaxis_transform()
        shape = [[end, tip_pos],
                [end, spine_pos],
                [start, spine_pos],
                [start, tip_pos],]
        rotation = 'horizontal'
        xtext = mean
        ytext = spine_pos-0.01
        va="top"
        ha="center"
        


        # add the bracket
    bracket = mpatches.PathPatch(
        mpath.Path(shape),
        transform=transform,
        clip_on=False,
        facecolor="none",
        edgecolor="k",
        linewidth=2,
    )
    ax.add_artist(bracket)

    # add the label
    txt = ax.text(
        xtext,
        ytext,
        label,
        ha=ha,va=va,
        rotation=rotation,
        clip_on=False,
        transform=transform,
    )

    return bracket, txt


def calculate_nodes(df, cpus_per_node):
    df['Nodes']=df['cores']/cpus_per_node
    df['SF/Node']=df['SF']/df['Nodes']
    df['Orbitals/Node']=df['Orbitals']/df['Nodes']


def minimum_dataframe(df,cpus_per_node,
                      walltime_key='Walltime',
                      groups=['Nat','Nodes']):
    """Find the minimum walltime upon runs having the same number of nodes"""
    mins=[]
    if 'Nodes' not in df.columns:
        calculate_nodes(df, cpus_per_node)
    for group,dft in df.groupby(groups):
        #print(nat,nodes)
        lowest=dft.index[dft[walltime_key].to_numpy().argmin()]
        #print(lowest)
        mins.append(lowest)
    return df.T[mins].T